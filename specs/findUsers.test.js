const axios = require('axios');
const chai = require('chai');
const expect = chai.expect;
const chaiResponseValidator = require('chai-openapi-response-validator');

chai.use(chaiResponseValidator('/home/samuelpereira/workspace/poc-api/saipos.yaml'));
 
describe('Stores', () => {
  it('Contract Test Stores', async () => {
    const res = await axios.get('http://localhost:3000/v1/stores?access_token=ei7C7LzCSIiUPNWkKCRPFiprDmUaFSLrjXvSqplCF3pTnGPaKPTfZtz9R2zXQYf8');
    expect(res.status).to.equal(200);

    expect(res).to.satisfyApiSpec;
  });
});